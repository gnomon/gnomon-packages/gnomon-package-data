import unittest

import numpy as np
import tempfile
import os 

import gnomon.core
from gnomon.core import gnomonDataDict, QVariant
from gnomon.utils import load_plugin_group

load_plugin_group("dataDictData")
load_plugin_group("dataDictReader")
load_plugin_group("dataDictWriter")


class TestGnomonDataDictIO(unittest.TestCase):

    def setUp(self):
        # -- Creating a simple dict to build up our gnomonDataDict
        self._dict = {}
        self._dict['int'] = 2
        self._dict['double'] = 2.5
        self._dict['string'] = "miaou"
        self._dict["int_arr"] = np.array([1,2,3,4,5], np.int32)
        self._dict["dbl_arr"] = np.array([1.0,1.1,1.2,1.3,1.4], np.float64)
        self._dict["int_arr2"] = np.array([[1,2,3,4,5], [6,7,8,9,10]], np.int32)

        # -- Creating a gnomonDataDict from a dict
        self.dataDict = gnomonDataDict()
        self.dataDict_data = gnomon.core.dataDictData_pluginFactory().create("gnomonNumpyDataDictData")
        self.dataDict_data.set_data(self._dict)
        self.dataDict.setData(self.dataDict_data)

        self.dataDict_series = {0: self.dataDict}

        # temp file
        self.temp_file = tempfile.NamedTemporaryFile(delete=False)
        self.temp_file.name += ".json"
        print("tempfile ", self.temp_file.name)

        # create a writer
        self.writer = gnomon.core.dataDictWriter_pluginFactory().create("gnomonDataDictWriterJson")
        self.writer.setDataDict(self.dataDict_series)
        self.writer.setPath(self.temp_file.name)

        # create a reader
        self.reader = gnomon.core.dataDictReader_pluginFactory().create("gnomonDataDictReaderJson")
        self.reader.setPath(self.temp_file.name)

    def tearDown(self):
        self.dataDict.this.disown()
        self.dataDict_data.this.disown()
        self.writer.this.disown()
        self.reader.this.disown()
        if os.path.exists(self.temp_file.name):
            os.remove(self.temp_file.name)


    def test_gnomonDataDictIO_write(self):
        self.writer.run()
        assert os.path.exists(self.temp_file.name)
        # TODO check file size


    def test_gnomonDataDictIO_read(self):
        self.writer.run()
        assert os.path.exists(self.temp_file.name)

        self.reader.run()

        read_data_series = self.reader.dataDict()
        read_data = read_data_series[0]
        print(read_data)
        for key in self._dict.keys():
            value = read_data.get(key)
            print("key: ", key, " value:", self._dict[key], " return value: ", value , type(value))
            if(type(value).__module__ == np.__name__):
                assert np.array_equal(value, self._dict[key])
            else:
                assert value == self._dict[key]


# test_gnomonDataDictIO.py ends here.
