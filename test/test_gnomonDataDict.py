import unittest

import numpy as np

import gnomon.core
from gnomon.core import gnomonDataDict
from gnomon.utils import load_plugin_group


load_plugin_group("dataDictData")


class TestGnomonDataDict(unittest.TestCase):

    def setUp(self):
        # -- Creating a simple dict to build up our gnomonDataDict
        self._dict = {}
        self._dict['int'] = 2
        self._dict['double'] = 2.5
        self._dict['string'] = "miaou"
        self._dict["int_arr"] = np.array([1,2,3,4,5], np.int32)
        self._dict["dbl_arr"] = np.array([1.0,1.1,1.2,1.3,1.4], np.float64)
        self._dict["int_arr2"] = np.array([[1,2,3,4,5], [6,7,8,9,10]], np.int32)
        aa = np.arange(120, dtype=np.int32)
        aa = np.reshape(aa, (5,4,3,2))
        self._dict["int_arr4"] = aa
        self.serialization_file = "test/resources/TestGnomonDataDictSerialization.txt"


        # -- Creating a gnomonDataDict from a dict
        self.dataDict = gnomonDataDict()
        self.dataDict_data = gnomon.core.dataDictData_pluginFactory().create("gnomonNumpyDataDictData")
        self.dataDict_data.set_data(self._dict)
        self.dataDict.setData(self.dataDict_data)

    def tearDown(self):
        self.dataDict.this.disown()
        self.dataDict_data.this.disown()

    def test_gnomonDataDict_keys(self):
        for key in self._dict.keys():
            assert key in self.dataDict.keys()

    def test_gnomonDataDict_get(self):
        for key in self._dict.keys():
            value = self.dataDict.get(key)
            print("key: ", key, " value:", self._dict[key], " return value: ", value , type(value))
            if(type(value).__module__ == np.__name__):
                assert np.array_equal(value, self._dict[key])
            else:
                assert value == self._dict[key]

    def test_serialization(self):
        serialization = self.dataDict.data().serialize()
        with open(self.serialization_file, "r") as file:
            control = file.read()
        assert control == serialization

    def test_deserialization(self):
        with open(self.serialization_file, "r") as file:
            control = file.read()
        new_form_data = gnomon.core.dataDictData_pluginFactory().create("gnomonNumpyDataDictData")
        new_form_data.deserialize(control)
        np.testing.assert_equal(new_form_data._data, self.dataDict.data()._data)


if __name__ == "__main__":
    t = TestGnomonDataDict()
    t.setUp()
    t.test_serialization()
    t.test_deserialization()
    #t.test_gnomonDataDict_get()
# test_gnomonDataDict.py ends here.
