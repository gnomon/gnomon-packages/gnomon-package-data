# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest

import gnomon.core
from gnomon.utils import load_plugin_group

load_plugin_group("dataFrameReader")


class TestGnomonDataFrameReader(unittest.TestCase):
    '''
    Tests the gnomonDataFrameReader class.
    '''

    def setUp(self):
        self.filename = "test/resources/E37_SAM7_t00_signal_data.csv"

        self.reader = gnomon.core.dataFrameReader_pluginFactory().create("gnomonDataFrameReaderPandas")
        self.reader.setPath(self.filename)

    def tearDown(self):
        self.reader.this.disown()

    def test_gnomonDataFrameReader_read(self):
        self.reader.run()
        dataFrame = self.reader.dataFrame()[0]
        assert dataFrame is not None

    def test_gnomonDataFrameReader_dataFrame(self):
        self.reader.run()
        dataFrame = self.reader.dataFrame()[0]
        assert "center_x" in dataFrame.columnNames()

#
# test_gnomonDataFrameReader.py ends here.
