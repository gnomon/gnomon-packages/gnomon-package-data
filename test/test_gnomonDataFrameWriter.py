# Version: $Id$
#
#

# Commentary:
#
#

# Change Log:
#
#

# Code:

import unittest
import os

import numpy as np

import gnomon.core
from gnomon.utils import load_plugin_group

load_plugin_group("dataFrameReader")
load_plugin_group("dataFrameWriter")


class TestGnomonDataFrameWriter(unittest.TestCase):
    '''
    Tests the gnomonDataFrameWriter class.
    '''

    def setUp(self):
        self.filename = "test/resources/E37_SAM7_t00_signal_data.csv"

        self.reader = gnomon.core.dataFrameReader_pluginFactory().create("gnomonDataFrameReaderPandas")
        self.reader.setPath(self.filename)

        self.reader.run()
        self.dataFrame = self.reader.dataFrame()

        self.saved_filename = "test/resources/dataFrame_writer_tmp.csv"

        self.writer = gnomon.core.dataFrameWriter_pluginFactory().create("gnomonDataFrameWriterPandas")
        self.writer.setPath(self.saved_filename)

    def tearDown(self):
        if os.path.exists(self.saved_filename):
            os.remove(self.saved_filename)
        self.reader.this.disown()
        self.writer.this.disown()

    def test_gnomonDataFrameWriter_write(self):
        self.writer.setDataFrame(self.dataFrame)
        self.writer.run()
        assert os.path.exists(self.saved_filename)

        self.reader.setPath(self.saved_filename)
        self.reader.run()
        read_dataFrame = self.reader.dataFrame()
        assert read_dataFrame is not None

        read_data = read_dataFrame[0]
        data = self.dataFrame[0]

        assert np.all([c in read_data.columnNames() for c in data.columnNames()])


#
# test_gnomonDataFrameWriter.py ends here.
