import unittest

import numpy as np
import pandas as pd

import gnomon.core
from gnomon.core import gnomonDataFrame
from gnomon.utils import load_plugin_group

load_plugin_group("dataFrameData")


class TestGnomonDataFrame(unittest.TestCase):

    def setUp(self):
        # -- Creating a pandas DataFrame to build up our gnomonDataFrame
        self._df = pd.DataFrame()
        self._df['x'] = np.array([0.0, 0.3, 0.5, 1., 4, 6, 7, 8.2])
        self._df['y'] = np.array([7., 3.3, 5.4, 7.6, 0.3, 4.7, 5.3, 8.8])

        # -- Creating a gnomonDataFrame from a Pandas dataframe
        self.dataFrame = gnomonDataFrame()
        self.dataFrame_data = gnomon.core.dataFrameData_pluginFactory().create("gnomonDataFrameDataPandas")
        self.dataFrame_data.set_dataframe(self._df)
        self.dataFrame.setData(self.dataFrame_data)

        self.serialization_file = "test/resources/TestGnomonDataFrameSerialization.txt"
        with open(self.serialization_file, "w") as file:  # prepare serialization resource
            file.write(self.dataFrame_data.serialize())


    def tearDown(self):
        self.dataFrame.this.disown()
        self.dataFrame_data.this.disown()

    def test_gnomonDataFrame_index(self):
        for line_id in self.dataFrame.index():
            assert line_id in self._df.index

    def test_gnomonDataFrame_columns(self):
        for column_name in self.dataFrame.columnNames():
            assert column_name in self._df

    def test_serialization(self):
        serialization = self.dataFrame.data().serialize()
        with open(self.serialization_file, "r") as file:
            control = file.read()
        assert control == serialization

    def test_deserialization(self):
        with open(self.serialization_file, "r") as file:
            control = file.read()
        new_form_data = gnomon.core.dataFrameData_pluginFactory().create("gnomonDataFrameDataPandas")
        new_form_data.deserialize(control)
        pd.testing.assert_frame_equal(new_form_data._df, self.dataFrame.data()._df)

# test_gnomonDataFrame.py ends here.
