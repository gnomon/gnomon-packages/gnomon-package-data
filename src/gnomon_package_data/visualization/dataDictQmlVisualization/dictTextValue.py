import os
import logging
from copy import deepcopy

import numpy as np
from dtkcore import d_inliststring

import gnomon.core
import gnomon.visualization
from gnomon.visualization import gnomonAbstractDataDictQmlVisualization
from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import dataDictInput


def _matrix_string(matrix):
    matrix_string = ""
    matrix_string += "["
    for row, row_values in enumerate(matrix):
        if row > 0:
            matrix_string += ",\n "
        matrix_string += " ["
        for col, value in enumerate(row_values):
            if col > 0:
                matrix_string += ","
            if value >=0:
                matrix_string += " "
            matrix_string += " " + '%.3f'%(value)
        matrix_string += "]"
    matrix_string += " ]"
    return matrix_string


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Dict Value")
@dataDictInput('data', "gnomonNumpyDataDictData")
class dictTextValue(gnomonAbstractDataDictQmlVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {
            'key': d_inliststring("Key", "", [""], "Key corresponding to the value to display")
        }

        self.data = {}

        self.text = ""

    def __del__(self):
        pass

    def clear(self):
        pass

    def fill(self):
        pass

    def setVisible(self, visible):
        pass

    def refreshParameters(self):
        if self.data is not None:
            data_dict = list(self.data.values())[-1]
            key = self['key']
            keys = list(data_dict.keys())
            self._parameters['key'].setValues(keys)
            if key in keys:
                self._parameters['key'].setValue(key)
            else:
                self._parameters['key'].setValue(keys[0])

    def update(self):
        current_time = self.view().currentTime()
        if current_time in self.data:
            data_dict = self.data[current_time]
            if self['key'] in data_dict:
                self.text = self['key'] + ":\n"
                value = data_dict[self['key']]
                if isinstance(value, str):
                    self.text += value
                elif isinstance(value, np.ndarray):
                    if value.ndim == 2:
                        self.text += _matrix_string(value)
                    else:
                        self.text += value.__str__()
                else:
                    self.text += value.__repr__()
            else:
                self.text = ""
        else:
            self.text = ""
        self.render()

    def render(self):
        if self.qmlView():
            self.qmlView().setDisplayText(self.text)

    def onTimeChanged(self, value):
        self.update()
