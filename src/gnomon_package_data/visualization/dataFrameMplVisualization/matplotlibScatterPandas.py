import os
import logging

import pandas as pd
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import Normalize, to_rgba
from mpl_toolkits.axes_grid1.inset_locator import inset_axes

from PIL import Image

from dtkcore import d_bool, d_int, d_inliststring, d_real, d_range_real, array_real_2

import gnomon.visualization
from gnomon.visualization import gnomonAbstractDataFrameMplVisualization
from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import dataFrameInput
from gnomon.utils.matplotlib_tools import gnomon_figure


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Scatter Data")
@dataFrameInput('df', data_plugin="gnomonDataFrameDataPandas")
class matplotlibScatterPandas(gnomonAbstractDataFrameMplVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {}

        self._parameters['all_times'] = d_bool("Merge all times", False, "Whether to merge all time points or consider them separately")

        self._parameters['X_variable'] = d_inliststring("X Variable", "", [""], "Data to plot on the X axis")
        self._parameters['Y_variable'] = d_inliststring("Y Variable", "", [""], "Data to plot on the Y axis")
        self._parameters['class_variable'] = d_inliststring("Classes", "", [""], "Property used to segregate data into clusters")
        self._parameters['class_colormap'] = gnomon.visualization.ParameterColorMap('colormap', 'viridis', 'Colormap for property')
        self._parameters['class_range'] = d_range_real("Class range", array_real_2([0., 1.]), 0., 1., "Value range for property display")
        self._parameters['alpha'] = d_real("Alpha", 1, 0, 1, 2, "Transparency of the displayed data")
        self._parameters['markersize'] = d_int("Markersize", 40, 0, 200, "Size of markers representing data points")
        self._parameters['max_classes'] = d_int("Max classes", 5, 0, 100, "Maximum number of different values to be considered as classes")

        self._parameters['filter_variable'] = d_inliststring("Filter", "", [""], "Property used to filter out data points")
        self._parameters['filter_range'] = d_range_real("Filter range", array_real_2([0., 1.]), 0., 1., "Value range for data filtering")

        self._parameters['show_regression'] = d_bool("Show regression", False, "Whether to display a regression of the data points")
        self._parameters['regression_type'] = d_inliststring("Regression type", "linear", ["linear"], "Type of regression to compute")

        self._parameter_groups = {}
        for parameter_name in ['class_variable', 'class_colormap', 'class_range', 'max_classes']:
            self._parameter_groups[parameter_name] = 'classes'
        for parameter_name in ['filter_variable', 'filter_range']:
            self._parameter_groups[parameter_name] = 'filter'
        for parameter_name in ['alpha', 'markersize']:
            self._parameter_groups[parameter_name] = 'rendering'
        for parameter_name in ['show_regression', 'regression_type']:
            self._parameter_groups[parameter_name] = 'regression'

        self.connectParameter("class_variable")
        self.connectParameter("filter_variable")
        self.connectParameter("all_times")

        self.df = {}

        self.current_time = None
        self.figure = None

    def __del__(self):
        if self.figure is not None:
            self.figure.clf()

    def refreshParameters(self):
        if len(self.df) > 0:
            df = list(self.df.values())[0]
            
            variables = [v for v in df.columns if not 'Unnamed:' in v]
            variables = [v for v in variables if not np.array(df[v]).dtype == np.dtype('O')]
            variables = [v for v in variables if np.array(df[v]).ndim == 1]
    
            for param in ['X_variable', 'Y_variable', 'filter_variable', 'class_variable']:
                if param == 'class_variable' and self['all_times']:
                    self._parameters[param].setValues([""] + variables + ['time'])
                else:
                    self._parameters[param].setValues([""] + variables)
    
                if param == 'X_variable' and len(variables)>0:
                    self._parameters[param].setValue(variables[0])
                elif param == 'Y_variable' and len(variables)>1:
                    self._parameters[param].setValue(variables[1])
                else:
                    self._parameters[param].setValue("")

    def onParameterChanged(self, parameter_name):
        if parameter_name in ['class_variable', 'filter_variable']:
            range_param = f"{parameter_name[:-9]}_range"

            if len(self.df) > 0:
                df = list(self.df.values())[0]
                if self[parameter_name]!="":
                    all_values = df[self[parameter_name]].values
                    value_list = np.sort(np.unique(all_values))

                    min_p = float(np.around(np.nanmin(all_values), decimals=3))
                    max_p = float(np.around(np.nanmax(all_values), decimals=3))

                    self._parameters[range_param].setMin(np.around(min_p - (max_p - min_p)/2, decimals=3))
                    self._parameters[range_param].setMax(np.around(max_p + (max_p - min_p)/2, decimals=3))
                    self._parameters[range_param].setValueMin(min_p)
                    self._parameters[range_param].setValueMax(max_p)

        if parameter_name in ['all_times']:
            values = self._parameters['class_variable'].values()
            if self['all_times']:
                values += ['time']
            else:
                values = values[:-1]
            self._parameters['class_variable'].setValues(values)

    def clear(self):
        if self.figure is not None:
            self.figure.clf()
            self.figure.canvas.draw()

    def setVisible(self, visible):
        pass

    def update(self):
        if len(self.df) == 0:
            return

        self.figure = gnomon_figure(self.figureNumber())

        self.current_time = self.view().currentTime()
        if self.current_time in self.df:
            if self['all_times']:
                time_dfs = []
                for time in self.df:
                    time_df = self.df[time]
                    time_df['time'] = time
                    time_dfs.append(time_df)
                df = pd.concat(time_dfs)
            else:
                df = self.df[self.current_time]

            self.figure.clf()

            variables = [v for v in df.columns if not 'Unnamed:' in v]
            variables = [v for v in variables if not np.array(df[v]).dtype == np.dtype('O')]
            variables = [v for v in variables if np.array(df[v]).ndim == 1]

            all_x = df[self['X_variable']].values if self['X_variable'] in variables else df.index
            all_y = df[self['Y_variable']].values if self['Y_variable'] in variables else df.index
            all_classes = df[self['class_variable']].values if self['class_variable'] in variables else np.array(["" for _ in df.index])
            all_filters = df[self['filter_variable']].values if self['filter_variable'] in variables else np.array(["" for _ in df.index])

            point_invalidity = np.zeros(len(all_x)).astype(bool)
            point_invalidity = point_invalidity | pd.isnull(all_x)
            point_invalidity = point_invalidity | pd.isnull(all_y)
            point_invalidity = point_invalidity | pd.isnull(all_classes)
            point_invalidity = point_invalidity | pd.isnull(all_filters)
            valid_points  = np.where(np.logical_not(point_invalidity))

            all_x = all_x[valid_points]
            all_y = all_y[valid_points]
            all_classes = all_classes[valid_points]
            all_filters = all_filters[valid_points]

            if self['filter_variable'] != "":
                filter_mask = np.ones(len(all_filters)).astype(bool)
                filter_mask = filter_mask & (all_filters >= self['filter_range'][0])
                filter_mask = filter_mask & (all_filters <= self['filter_range'][1])

                all_x = all_x[filter_mask]
                all_y = all_y[filter_mask]
                all_classes = all_classes[filter_mask]
                all_filters = all_filters[filter_mask]

            colormap = self._parameters['class_colormap'].name()

            class_list = np.sort(np.unique(all_classes))

            if class_list.dtype == np.dtype('O') or len(class_list) <= self['max_classes']:
                for i_c, c in enumerate(class_list):
                    color = cm.ScalarMappable(cmap=colormap, norm=Normalize(0, len(class_list)-1)).to_rgba(i_c)
                    label = "".join([w.capitalize() + " " for w in self["class_variable"].split('_')]) + str(c)

                    class_x = all_x[all_classes==c]
                    class_y = all_y[all_classes==c]

                    self.figure.gca().scatter(
                        class_x, class_y,
                        s=self['markersize'],
                        color=color, ec='k', alpha=self['alpha'],
                        label=label if len(class_list)>1 else None
                    )

                    if self["show_regression"]:
                        if self["regression_type"] == 'linear':
                            class_reg = np.polyfit(class_x, class_y, deg=1)
                            reg_x = np.array([np.nanmin(all_x), np.nanmax(all_x)])
                            reg_y = np.polyval(class_reg, reg_x)
                            r2 = 1 - np.nanmean(np.square(np.polyval(class_reg, class_x) - class_y))/np.nanvar(class_y)
                            self.figure.gca().plot(reg_x, reg_y, color=color, alpha=self['alpha'], label=f"$r^2 = {np.round(r2, 2)}$")

                if len(class_list)>1 or self["show_regression"]:
                    self.figure.gca().legend(loc="lower right")
            else:
                value_range = self["class_range"]

                col = self.figure.gca().scatter(
                    all_x, all_y, c=all_classes,
                    s=self['markersize'],
                    cmap=colormap, ec='k', alpha=self['alpha'],
                    vmin=value_range[0], vmax=value_range[1]
                )

                if self["show_regression"]:
                    if self["regression_type"] == 'linear':
                        reg = np.polyfit(all_x, all_y, deg=1)
                        reg_x = np.array([np.nanmin(all_x), np.nanmax(all_x)])
                        reg_y = np.polyval(reg, reg_x)
                        r2 = 1 - np.nanmean(np.square(np.polyval(reg, all_x) - all_y))/np.nanvar(all_y)
                        self.figure.gca().plot(reg_x, reg_y, color='k', alpha=self['alpha'], label=f"$r^2 = {np.round(r2, 2)}$")

                    self.figure.gca().legend(loc="lower right")

                if col is not None:
                    ax =  self.figure.gca()
                    cax = inset_axes(self.figure.gca(), width="3%", height="25%", loc='center right')
                    cbar = self.figure.colorbar(col, cax=cax, pad=0.)
                    cax.set_title(self['class_variable'], size=10)
                    cax.yaxis.set_ticks_position('left')
                    self.figure.sca(ax)

            self.figure.gca().set_xlabel(self['X_variable'], size=12)
            self.figure.gca().set_ylabel(self['Y_variable'], size=12)

        self.render()

    def render(self):
        if self.figure is not None:
            self.figure.tight_layout()
            self.figure.canvas.draw()

    def imageRendering(self):
        if self.figure is not None:
            w, h = self.figure.canvas.get_width_height()

            canvas_img = np.fromstring(self.figure.canvas.tostring_rgb(), dtype=np.uint8, sep='')
            if (canvas_img.shape[0]/3) == 4*h*w:
                canvas_img = canvas_img.reshape(2*h, 2*w, 3)
            elif (canvas_img.shape[0]/3) == (2*h)*(2*w+1):
                canvas_img = canvas_img.reshape(2*h, 2*w+1, 3)
            elif (canvas_img.shape[0]/3) == (2*h+1)*(2*w):
                canvas_img = canvas_img.reshape(2*h+1, 2*w, 3)
            elif (canvas_img.shape[0]/3) == h*w:
                canvas_img = canvas_img.reshape(h, w, 3)
            else:
                canvas_img = np.zeros((600, 600, 3), float)

            canvas_img = np.array(Image.fromarray(canvas_img).resize((600, 600)))
        else:
            canvas_img = np.zeros((600, 600, 3), float)

        self.image = [[[rgb for rgb in c] for c in r] for r in canvas_img]
        return self.image

    def onTimeChanged(self, value):
        if value in self.df:
            time_changed =  value != self.current_time
            self.current_time = value
            if time_changed:
                self.update()
