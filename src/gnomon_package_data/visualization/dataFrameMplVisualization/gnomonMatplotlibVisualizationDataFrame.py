import os
import logging

import numpy as np
import numpy.linalg as lng
import matplotlib as mpl
from matplotlib import cm
from matplotlib.colors import Normalize, to_rgba
from matplotlib.patches import Ellipse
# from scipy.misc import imresize
from PIL import Image

from dtkcore import d_bool, d_int, d_inliststring, d_real

import gnomon.core
# import gnomonwidgets
import gnomon.visualization
from gnomon.visualization import gnomonAbstractDataFrameMplVisualization

import pandas as pd
import numpy as np

from gnomon.utils import load_plugin_group, visualizationPlugin
from gnomon.utils.matplotlib_tools import gnomon_figure


def draw_box(figure, signal_values, box_x=0., box_width=0.8333, color='none', box_color='k', linewidth=2, alpha=1, outlier_size=20, outlier_alpha=0.33):
    """
    """

    signal_time_percentiles = {}
    for percentile in [10, 25, 50, 75, 90]:
        signal_time_percentiles[percentile] = np.nanpercentile(signal_values, percentile)
        signal_time_mean = np.nanmean(signal_values)

    figure.gca().fill_between([box_x - box_width, box_x + box_width], [signal_time_percentiles[75], signal_time_percentiles[75]], [signal_time_percentiles[25], signal_time_percentiles[25]],
                              facecolor=color, alpha=alpha)
    figure.gca().plot([box_x - box_width, box_x + box_width], [signal_time_percentiles[50], signal_time_percentiles[50]], color=box_color, linewidth=1.5 * linewidth, alpha=alpha)
    figure.gca().plot([box_x - box_width, box_x + box_width], [signal_time_percentiles[25], signal_time_percentiles[25]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x - box_width, box_x + box_width], [signal_time_percentiles[75], signal_time_percentiles[75]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x - box_width, box_x - box_width], [signal_time_percentiles[25], signal_time_percentiles[75]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x + box_width, box_x + box_width], [signal_time_percentiles[25], signal_time_percentiles[75]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x, box_x], [signal_time_percentiles[25], signal_time_percentiles[10]], color=box_color, linewidth=linewidth / 2., linestyle='dashed', alpha=alpha)
    figure.gca().plot([box_x, box_x], [signal_time_percentiles[75], signal_time_percentiles[90]], color=box_color, linewidth=linewidth / 2., linestyle='dashed', alpha=alpha)
    figure.gca().plot([box_x - box_width / 5., box_x + box_width / 5.], [signal_time_percentiles[10], signal_time_percentiles[10]], color=box_color, linewidth=linewidth / 2., alpha=alpha)
    figure.gca().plot([box_x - box_width / 5., box_x + box_width / 5.], [signal_time_percentiles[90], signal_time_percentiles[90]], color=box_color, linewidth=linewidth / 2., alpha=alpha)

    outliers = [d for d in signal_values if (d < signal_time_percentiles[10]) or (d > signal_time_percentiles[90])]
    outliers_x = [box_x for d in outliers]
    figure.gca().scatter(outliers_x, outliers, s=outlier_size, facecolor='none', edgecolor='k', linewidth=1, alpha=outlier_alpha)


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Generic Data Plot")
class gnomonMatplotlibVisulalizationDataFrame(gnomonAbstractDataFrameMplVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {}
        # self._parameters['figure_numbers']  = d_int(0, 0, 20, "Indices of the various figures to plot")
        self._parameters['plot'] = d_inliststring("Plot", "scatter", ["scatter", "hist", "cumulative", "plot", "boxplot", "violin", "clusters"], "Type of plot for plotting the data")
        self._parameters['X_variable'] = d_inliststring("X", "", [""], "Data to plot on the X axis of the figure")
        self._parameters['Y_variable'] = d_inliststring("Y", "", [""], "Data to plot on the Y axis of the figure")
        self._parameters['class_variable'] = d_inliststring("Classes", "", [""], "Property used to segregate data into clusters")
        # self._parameters['X_unit'] = gnomon.core.ParameterText("X_unit", "The unit to display along the X variable name in labels", "A.U.")
        # self._parameters['Y_unit'] = gnomon.core.ParameterText("Y_unit", "The unit to display along the Y variable name in labels", "A.U.")
        # self._parameters['class_unit'] = gnomon.core.ParameterText("class_unit", "The unit to display along the class variable name in labels", "A.U.")
        self._parameters['markersize'] = d_int("Markersize", 40, 0, 200, "Size of markers representing data points")
        # self._parameters['linestyle'] = d_inliststring("linestyle", ":"], "type of line between markers representing data points", "-", ["-", "--", "-.")
        self._parameters['linewidth'] = d_int("Linewidth", 1, 0, 10, "Width of lines in the representation of data")
        self._parameters['alpha'] = d_real("Alpha", 1, 0, 1, 2, "Transparency of the displayed data")
        # self._parameters['color'] = d_inliststring("color", "m"], "Color of the plot", "b", ["k", "r", "g", "b", "y", "c")
        # self._parameters['marker'] = d_inliststring("marker", "+"], "Glyph representing data points", "o", ["o", "s", "^", "*", ">", "<")
        # self._parameters['resolution'] = d_int(10, 1, 100, "Resolution of the plot")
        # self._parameters['labelsize'] = d_int(15, 1, 30, "Size of the various labels of the graph")
        self._parameters['class_colormap'] = gnomon.visualization.ParameterColorMap('colormap', 'viridis', 'Colormap for property')
        self._parameters['legend_position'] = d_inliststring("Legend position", "upper left", ["upper left", "upper right", "lower right", "lower left"], "Position of the legend")
        self._parameters['display_legend'] = d_bool("display_legend", True, "If True, a legend of the various classes is displayed")
        # self._parameters['display_grid'] = d_inliststring("display_grid", "y"], "Display a visual guide on the figure", "no", ["no", "both", "x")
        # self._parameters['caption'] = gnomon.core.ParameterText("caption", "A concise description of the figure that could be used latter on.", "Add caption")
        # self._parameters['font_type'] = d_inliststring("font_type", "latex_sans_serif"], "Font rendering on the figure.", "classic_mpl", ["classic_mpl", "latex_serif")

        self.df = None
        self._dataFrame = None

        self.figure = None

    def __del__(self):
        self.figure.clf()

    def setDataFrame(self, dataFrame):
        self._dataFrame = dataFrame
        self.df = self._dataFrame.data()._df
        self.refreshParameters()

    def dataFrame(self):
        print(self._dataFrame)
        return self._dataFrame

    def refreshParameters(self):
        for variable_type in ['X', 'Y', 'class']:
            variables = [v for v in self.df.columns if not 'Unnamed:' in v]
            variables = [v for v in variables if not np.array(self.df[v]).dtype == np.dtype('O')]
            variables = [v for v in variables if np.array(self.df[v]).ndim == 1]

            self._parameters[variable_type + '_variable'].setValues([""] + variables)

            if variable_type == 'X' and len(variables)>0:
                self._parameters[variable_type + '_variable'].setValue(variables[0])
            elif variable_type == 'Y' and len(variables)>1:
                self._parameters[variable_type + '_variable'].setValue(variables[1])
            else:
                self._parameters[variable_type + '_variable'].setValue("")

    def clear(self):
        if self.figure is None:
            self.figure = plt.figure()
        self.figure.clf()
        self.figure.canvas.draw()

    def update(self):
        if self._dataFrame is None:
            return

        # -- Use Latex in the various labels and captions.
        # font_type = self['font_type']
        #
        # if font_type in ["latex_serif", "latex_sans_serif"]:
        #     mpl.rcParams['text.usetex'] = True
        #     mpl.rcParams['text.latex.preamble'] = [r'\usepackage[cm]{sfmath}']
        #     mpl.rcParams['font.family'] = 'sans-serif'
        #     mpl.rcParams['font.sans-serif'] = 'cm'
        #     # plt.rc('text', usetex=True)
        #
        #     if font_type=="latex_serif":
        #
        #         plt.rc('font', family='serif', serif='Palatino') # possible serif fonts: Times, Palatino, New Century Schoolbook, Bookman, Computer Modern Roman
        #     else:
        #         plt.rc('font', family='sans-serif') # possible sans-serif fonts: Helvetica, Avant Garde, Computer Modern Sans serif

        # -- Generate the figure
        # self.figure = plt.figure(self['figure_numbers'])
        self.figure = gnomon_figure(self.figureNumber())
        self.figure.clf()

        # -- Extract the variables out of the panda dataframe structure
        variables = [v for v in self.df.keys() if not 'Unnamed:' in v]
        variables = [v for v in variables if not np.array(self.df[v]).dtype == np.dtype('O')]
        all_data  = np.transpose([self.df[variable] for variable in variables])

        # -- Defining and formatting the variables to plot
        all_X = self.format_variable('X_variable')
        all_Y = self.format_variable('Y_variable')
        all_C = self.format_variable('class_variable')

        # -- Remove the points where the X, Y & class values are nan
        point_invalidity = np.zeros(len(all_X)).astype(bool)
        point_invalidity = point_invalidity | pd.isnull(all_X)
        point_invalidity = point_invalidity | pd.isnull(all_Y)
        point_invalidity = point_invalidity | pd.isnull(all_C)
        valid_points     = np.where(np.logical_not(point_invalidity))

        all_data = all_data[valid_points]
        all_X    = all_X[valid_points]
        all_Y    = all_Y[valid_points]
        all_C    = all_C[valid_points]

        X = all_X
        Y = all_Y
        C = all_C

        X_variable = self['X_variable']
        Y_variable = self['Y_variable']
        C_variable = self['class_variable']


        # -- Defining the various classes within the data
        class_list  = np.sort(np.unique(all_C))


        # -- Usefull plotting variables
        resolution = 10 #self['resolution']
        marker     = "o" #self['marker']
        markersize = self['markersize']
        linewidth  = self['linewidth']
        linestyle  = "-" #self['linestyle']
        alpha      = self['alpha']


        # -- Needed for boxplots and violin plots
        if self['plot'] in ['hist', 'boxplot', 'violin']:
            datas, colors, labels = [], [], []


        # -- Compute same bins for all histograms
        if self['plot'] in ['hist', 'cumulative']:
            _, bins, _ = self.figure.gca().hist(X, bins=resolution)
            self.figure.clf()


        # -- Drawing the various types of plots
        for i_c, cls in enumerate(class_list):
            # --*-- Formatting and labelling of the various classes
            color, edge_color, label = self.format_plot(cls, class_list)

            # --*-- Plots
            if self['plot']=='plot':
                # --*--*-- Sorting the values so we draw a curve
                Ybis = Y[C==cls]
                x_data = np.sort(X[C==cls])
                y_data = Ybis[np.argsort(X[C==cls])]

                self.figure.gca().plot(x_data, y_data, c=color,
                                                       linewidth=linewidth,
                                                       linestyle=linestyle,
                                                       alpha=alpha,
                                                       label=label)

            # --*-- Scatter plots
            if self['plot']=='scatter':
                self.figure.gca().scatter(X[C==cls], Y[C==cls], marker=marker,
                                                                s=markersize,
                                                                c=color,
                                                                ec=edge_color,
                                                                linewidth=linewidth,
                                                                alpha=alpha,
                                                                label=label)

            # --*-- Histograms
            if self['plot']=='hist':
                datas, colors, labels = self.construct_data_2_plot(X[C==cls],
                                                                   color,
                                                                   label,
                                                                   datas,
                                                                   colors=colors,
                                                                   labels=labels)

                if cls==class_list[-1]:
                    self.figure.gca().hist(datas, bins=bins,
                                                        stacked=True,
                                                        color=colors,
                                                        alpha=alpha,
                                                        label=labels)

            # --*-- Cumulative histograms
            if self['plot']=='cumulative':
                _, _, patches =self.figure.gca().hist(X[C==cls],
                                                      bins=bins,
                                                      density=True,
                                                      cumulative=True,
                                                      histtype='step',
                                                      linewidth=linewidth,
                                                      color=color,
                                                      alpha=alpha,
                                                      label=label)

                # --*--*-- Remove the vertical bar on the right of the plot
                patches[0].set_xy(patches[0].get_xy()[:-1])

            # --*-- Boxplots
            if self['plot']=='boxplot':

                draw_box(self.figure,
                         X[C==cls],
                         box_x=i_c+1,
                         box_width=0.333,
                         box_color='k',
                         color=color,
                         linewidth=linewidth,
                         outlier_size=markersize)

                # datas, _, _ = self.construct_data_2_plot(X[C==cls],
                #                                         color,
                #                                         label,
                #                                         datas)
                #
                # if cls==class_list[-1]:
                #     self.figure.gca().boxplot(datas)

            # --*-- Violinplots
            if self['plot']=='violin':
                datas, colors, _ = self.construct_data_2_plot(X[C==cls],
                                                                   color,
                                                                   label,
                                                                   datas,
                                                                   colors=colors)

                if cls==class_list[-1]:
                    violin_ax = self.figure.gca().violinplot(datas)

                    # --*--*-- Adding markers on medians and quartiles
                    self.add_quartiles_2_violinplot(datas)

                    # --*--*-- Tweaking the violin plots style a bit
                    for i, pc in enumerate(violin_ax['bodies']):
                        pc.set_facecolor(colors[i])
                        pc.set_alpha(.9*alpha)

            # --*-- Cluster plots
            if self['plot']=='clusters':
                self.plot_fireworks(X[C==cls],
                                    Y[C==cls], marker=marker,
                                               markersize=markersize,
                                               color=color,
                                               alpha=alpha,
                                               label=label)




        # -- Setting the plot limits
        self.update_plot_limits(X, Y, C)


        # -- Putting labels on the various axes
        self.update_plot_labels(class_list)


        self.render()


    def render(self):
        if self.figure is not None:
            self.figure.canvas.draw()

    def imageRendering(self):
        if self.figure is not None:
            w, h = self.figure.canvas.get_width_height()

            canvas_img = np.fromstring(self.figure.canvas.tostring_rgb(), dtype=np.uint8, sep='')
            if (canvas_img.shape[0]/3) == 4*h*w:
                canvas_img = canvas_img.reshape(2*h, 2*w, 3)
            elif (canvas_img.shape[0]/3) == (2*h)*(2*w+1):
                canvas_img = canvas_img.reshape(2*h, 2*w+1, 3)
            elif (canvas_img.shape[0]/3) == (2*h+1)*(2*w):
                canvas_img = canvas_img.reshape(2*h+1, 2*w, 3)
            elif (canvas_img.shape[0]/3) == h*w:
                canvas_img = canvas_img.reshape(h, w, 3)
            else:
                canvas_img = np.zeros((600, 600, 3), float)

            canvas_img = np.array(Image.fromarray(canvas_img).resize((600, 600)))
        else:
            canvas_img = np.zeros((600, 600, 3), float)

        self.image = [[[rgb for rgb in c] for c in r] for r in canvas_img]
        return self.image


    # -- Extra usefull methods
    def format_variable(self, variable_name):
        """Formats a variable from the dataframe into a numpy array.

        Parameters
        ----------
        variable_name : str
            The name of a data in the dataframe.
        Returns
        -------
        formatted_variable : ndarray
            An array containing the values to plot.
        """
        variable = self[variable_name]
        if variable != "":
            formatted_variable = self.df[variable].values
        elif variable_name=="class_variable":
            formatted_variable = np.zeros(len(self.df))
        else:
            formatted_variable = np.array(self.df.index)

        return formatted_variable

    def update_plot_limits(self, X, Y, C):
        """Scale properly the x, y-axes.

        Parameters
        ----------
        self : gnomonMatplotlibVisulalizationDataFrame
            the instance of gnomon plot we are working on.
        X, Y, C : ndarray
            arrays of data to plot.

        Returns
        -------
        None.
        """
        if self['plot'] not in ['boxplot', 'violin']:
            x_range = (X.min(), X.max())

        else:
            nbr_cls = len(np.unique(C))
            x_range = (0, nbr_cls+1)

        self.figure.gca().set_xlim(*x_range)


        if self['plot'] not in ['hist', 'cumulative', 'boxplot', 'violin']:
            y_range = (Y.min(), Y.max())
            self.figure.gca().set_ylim(*y_range)

        elif self['plot'] in ['boxplot', 'violin']:
            y_range = (X.min()-2, X.max()+2)
            self.figure.gca().set_ylim(*y_range)

        self.figure.gca().ticklabel_format(axis='both', style='sci',
                                                        scilimits=(0, 1))



    def update_plot_labels(self, class_list):
        """Set the various label names.

        Parameters
        ----------
        self : gnomonMatplotlibVisulalizationDataFrame
            the instance of gnomon plot we are working on.

        class_list: list(class_variable_type)
            The list of class values used within the figure.

        Returns
        -------
        None.

        """
        X_var_name = self['X_variable']
        Y_var_name = self['Y_variable']
        C_var_name = self['class_variable']
        labelsize  = 15 #self['labelsize']
        legendpos  = self['legend_position']
        legendon   = self['display_legend']
        # X_unit     = self['X_unit']
        # Y_unit     = self['Y_unit']
        # C_unit     = self['class_unit']


        # -- Setting the labels of the axes
        xlabel = "".join([w.capitalize() + " " for w in X_var_name.split('_')])
        xunit  = "" #"" ("+X_unit+")"
        ylabel = "".join([w.capitalize() + " " for w in Y_var_name.split('_')])
        yunit  = "" #"" ("+Y_unit+")"
        clabel = "".join([w.capitalize() + " " for w in C_var_name.split('_')])
        cunit  = "" #"" ("+C_unit+")"

        if self['plot'] not in ['boxplot', 'violin']:
            self.figure.gca().set_xlabel(xlabel+xunit, size=labelsize)
        else:
            self.figure.gca().set_xlabel(clabel, size=labelsize)
            self.figure.gca().set_xticks(np.arange(1, len(class_list)+1))
            self.figure.gca().set_xticklabels(class_list, size=labelsize)

        if self['plot'] not in ['hist', 'cumulative', 'boxplot', 'violin']:
            self.figure.gca().set_ylabel(ylabel+yunit, size=labelsize)
        elif self['plot'] in ['boxplot', 'violin']:
            self.figure.gca().set_ylabel(xlabel+xunit, size=labelsize)


        # -- Setting the title of the graph
        if self['plot']=='hist':
            title = xlabel + " distribution"
        elif self['plot']=='cumulative':
            title = xlabel + " cumulative, normalized distribution"
        elif self['plot'] in ['boxplot', 'violin']:
            title = xlabel + " in various " + clabel
        else:
            title = ylabel + " vs " + xlabel

        self.figure.gca().set_title(title, size=int(1.2*labelsize), pad=20)

        # -- Setting the legend
        if legendon:
            self.figure.gca().legend(loc=legendpos)



    def format_plot(self, cls, class_list):
        """Generates the colors, the labels and the grid for the figure.

        Parameters
        ----------
        cls: float
            the current value of the considered class.

        class_list : ndarray
            the values of the variable used to define the various classes.

        Returns
        -------
        color: str or matplotlib colormap
            the color to use for the plots

        label: dict()
            - keys : float. The values defined within the class_list argument
            - values : str. The name associated with each class.

        """
        # -- Labelling the various classes
        nbr_classes = len(class_list)
        C_var_name  = self['class_variable']
        classname   = "".join([w.capitalize() + " " for w in C_var_name.split('_')])
        classlabel  = {cls: classname + str(cls) for cls in class_list}

        class_min = class_list.min()
        class_max = class_list.max()

        if nbr_classes>1:
            clscmap = self._parameters['class_colormap'].name()
            if class_list.dtype != np.dtype('O'):
                color   = cm.ScalarMappable(cmap=clscmap, norm=Normalize(class_min, class_max)).to_rgba(cls)
            else:
                color   = cm.ScalarMappable(cmap=clscmap, norm=Normalize(0, len(class_list))).to_rgba(np.where(class_list==cls)[0][0])
            edge_color = "k" #self['color']
        else:
            color   = "b" #self['color']
            edge_color = "k"

        label = classlabel[cls]

        # -- Generating the grid
        display_grid = 'no' #self['display_grid']
        if display_grid!='no':
            self.figure.gca().grid(True, axis=display_grid)

        return color, edge_color, label


    def construct_data_2_plot(self, data, color, label, datas, colors=None, labels=None):
        """Builds useful lists to plot some types of graphs.

        Parameters
        ----------
        data : ndarray(float)
            The data to add to the series to plot
        color : matplotlib color
            The color associated with the data to plot.
        label : str
            The label associated with the data to plot.
        datas : list(ndarray(float))
            The list of data to plot.
        colors : list(matplotlib color)
            The list of corresponding colors.
        labels : list(str)
            The list of corresponding labels.

        Returns
        -------
        datas : list(ndarray(float))
            The list of data to plot.
        colors : list(matplotlib color)
            The list of corresponding colors.
        labels : list(str)
            The list of corresponding labels.

        """
        datas.append(data)

        if colors!=None:
            colors.append(color)
        if labels!=None:
            labels.append(label)

        return datas, colors, labels



    def add_quartiles_2_violinplot(self, datas):
        for quartile, marker in [(25, '^'), (50, 'd'), (75, 'v')]:
            mrkr_pos = [np.percentile(data, quartile)for data in datas]
            inds     = np.arange(1, len(datas) + 1)

            self.figure.gca().scatter(inds, mrkr_pos, marker=marker,
                                                      color='w',
                                                      s=30,
                                                      zorder=3)



    def plot_fireworks(self, Xvar, Yvar, marker='o',
                                         markersize=10,
                                         color='k',
                                         alpha=1,
                                         label='no label'):
        """Plots a firework-like representation of a 2D point cloud.

        Parameters
        ----------
        Xvar : ndarray(float)
            One variable to consider.
        Yvar : ndarray(float)
            The other variable to consider.
        marker : str
            Optional (default : 'o'). Shape of the marker to use for the point
            cloud.
        markersize : int
            Optional (default : 10). size of the star representing
            the center of the point cloud.
        color : matplotlib color
            Optional (default : 'k'). Color of the representation.
        alpha: float
            Optional (default : 1). Between 0 and 1. Set the opacity of the
            ellipse.
        label : str
            Optional (default : 'no label').
            Name of the point cloud.

        Returns
        -------
        None

        """

        vctrs = np.array([np.array([x, y]) for x, y in zip(Xvar, Yvar)])
        cntr  = np.mean(vctrs, axis=0)

        # -- Plotting the lines between barycenter and each point of the cloud
        for x, y in zip(Xvar, Yvar):
            self.figure.gca().plot([cntr[0], x],
                                   [cntr[1], y], c=color,
                                                 linewidth=.5,
                                                 alpha=.5)

        # -- Computing the variance-covariance matrix of each point cloud
        mtrxs = np.array([np.outer(vctr - cntr, vctr - cntr)for vctr in vctrs])
        covar = np.mean(mtrxs, axis=0)

        eigvals, eigvcts = lng.eigh(covar)
        eigvcts = eigvcts.transpose() # WARNING: The vectors as displayed in columns in the eigh output.

        # -- Updating the label so we get distribution information in the legend
        label += " : (% .3g, % .3g) $\pm$ (% .3g, % .3g)" %(cntr[0], cntr[1], eigvals[0], eigvals[1])


        # -- Angle between first eigenvector and the x-axis
        angle = (180 / np.pi) * np.arctan(eigvcts[0][1] /
                                          eigvcts[0][0])

        # -- Drawing ellipses
        ellipse = Ellipse(xy=(cntr[0],
                              cntr[1]), width=2*np.sqrt(eigvals[0]),
                                        height=2*np.sqrt(eigvals[1]),
                                        angle=angle,
                                        facecolor=to_rgba(color, alpha=.33*alpha),
                                        linewidth=self['linewidth'],
                                        linestyle= "-", #self['linestyle'],
                                        edgecolor=color)

        self.figure.gca().add_artist(ellipse)

        # -- Plotting the barycenter of each point cloud associated to a specific class
        self.figure.gca().scatter(cntr[0],
                                  cntr[1], marker='*',
                                           s=10*markersize,
                                           c=color,
                                           edgecolors='w',
                                           alpha=1,
                                           zorder=6)

        # -- Plotting the point cloud as a scatter plot
        self.figure.gca().scatter(Xvar, Yvar, marker=marker,
                                              s=.5*markersize,
                                              c=color,
                                              linewidth=0,
                                              alpha=.5,
                                              zorder=1,
                                              label=label)


#
## gnomonMatplotlibVisulalizationDataFrame.py ends here.
