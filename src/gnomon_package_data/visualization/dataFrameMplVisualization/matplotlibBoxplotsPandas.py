import os
import logging

import pandas as pd
import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import Normalize, to_rgba

from PIL import Image

from dtkcore import d_bool, d_int, d_inliststring, d_real

import gnomon.visualization
from gnomon.visualization import gnomonAbstractDataFrameMplVisualization
from gnomon.utils import visualizationPlugin
from gnomon.utils.decorators import dataFrameInput
from gnomon.utils.matplotlib_tools import gnomon_figure


@visualizationPlugin(version="1.0.0", coreversion="1.0.0", name="Boxplot Data")
@dataFrameInput('df', data_plugin="gnomonDataFrameDataPandas")
class matplotlibBoxplotsPandas(gnomonAbstractDataFrameMplVisualization):
    def __init__(self):
        super().__init__()

        self._parameters = {}
        self._parameters['all_times'] = d_bool("Merge all times", False, "Whether to merge all time points or consider them separately")
        self._parameters['variable'] = d_inliststring("Variable", "", [""], "Data to plot in the boxplots")
        self._parameters['class_variable'] = d_inliststring("Classes", "", [""], "Property used to segregate data into clusters")
        self._parameters['class_colormap'] = gnomon.visualization.ParameterColorMap('Colormap', 'viridis', 'Colormap for property')
        self._parameters['violin'] = d_bool("Violin Plot", False, 'Whether to display violin plots instead of boxes')

        self.connectParameter("all_times")

        self.df = {}
        
        self.current_time = None
        self.figure = None

    def __del__(self):
        if self.figure is not None:
            self.figure.clf()

    def refreshParameters(self):
        if len(self.df) > 0:
            df = list(self.df.values())[0]
            
            variables = [v for v in df.columns if not 'Unnamed:' in v]
            variables = [v for v in variables if not np.array(df[v]).dtype == np.dtype('O')]
            variables = [v for v in variables if np.array(df[v]).ndim == 1]
    
            for param in ['variable', 'class_variable']:
                if param == 'class_variable' and self['all_times']:
                    self._parameters[param].setValues([""] + variables + ['time'])
                else:
                    self._parameters[param].setValues([""] + variables)
    
                if param == 'variable' and len(variables)>0:
                    self._parameters['variable'].setValue(variables[0])
                else:
                    self._parameters[param].setValue("")

    def onParameterChanged(self, parameter_name):
        if parameter_name in ['all_times']:
            values = self._parameters['class_variable'].values()
            if self['all_times']:
                values += ['time']
            else:
                values = values[:-1]
            self._parameters['class_variable'].setValues(values)

    def clear(self):
        if self.figure is not None:
            self.figure.clf()
            self.figure.canvas.draw()

    def setVisible(self, visible):
        pass

    def update(self):
        if len(self.df) == 0:
            return

        self.figure = gnomon_figure(self.figureNumber())

        self.current_time = self.view().currentTime()
        if self.current_time in self.df:
            if self['all_times']:
                time_dfs = []
                for time in self.df:
                    time_df = self.df[time]
                    time_df['time'] = time
                    time_dfs.append(time_df)
                df = pd.concat(time_dfs)
            else:
                df = self.df[self.current_time]

            self.figure.clf()
            ax = self.figure.gca()
    
            variables = [v for v in df.columns if not 'Unnamed:' in v]
            variables = [v for v in variables if not np.array(df[v]).dtype == np.dtype('O')]
            variables = [v for v in variables if np.array(df[v]).ndim == 1]
    
            all_variables = df[self['variable']].values if self['variable'] in variables else df.index
            all_classes = df[self['class_variable']].values if self['class_variable'] in variables else np.array(["" for _ in df.index])
    
            point_invalidity = np.zeros(len(all_variables)).astype(bool)
            point_invalidity = point_invalidity | pd.isnull(all_variables)
            point_invalidity = point_invalidity | pd.isnull(all_classes)
            valid_points  = np.where(np.logical_not(point_invalidity))
    
            all_variables = all_variables[valid_points]
            all_classes = all_classes[valid_points]
    
            colormap = self._parameters['class_colormap'].name()
    
            class_list = np.sort(np.unique(all_classes))
            for i_c, c in enumerate(class_list):
                color = cm.ScalarMappable(cmap=colormap, norm=Normalize(0, len(class_list)-1)).to_rgba(i_c)
                edge_color = "k"
    
                class_variables = all_variables[all_classes==c]
                if not self['violin']:
                    ax.boxplot(
                        class_variables,
                        positions=[i_c], widths=[0.66],
                        medianprops={'color': edge_color, 'linewidth': 2},
                        flierprops={'markersize': 5, 'alpha': 0.33},
                        patch_artist=True, boxprops={'facecolor': color, 'edgecolor': edge_color}
                    )
                else:
                    parts = ax.violinplot(
                        class_variables,
                        positions=[i_c], widths=[0.66],
                        showmedians=True, showextrema=True
                    )
                    for pc in parts['bodies']:
                        pc.set_facecolor(color)
                        pc.set_edgecolor(edge_color)
                        pc.set_alpha(1)
                    for part in ['cbars', 'cmedians', 'cmins', 'cmaxes']:
                        parts[part].set_color(edge_color)
                        parts[part].set_linewidth(2 if part == 'cmedians' else 1)

            ax.set_xlim(-1, len(class_list))
            ax.set_xticks(range(len(class_list)))
            ax.set_xticklabels([str(c) for c in class_list], size=8)
            ax.set_xlabel(self['class_variable'], size=12)
    
            ax.set_ylabel(self['variable'], size=12)

        self.render()

    def render(self):
        if self.figure is not None:
            self.figure.tight_layout()
            self.figure.canvas.draw()

    def imageRendering(self):
        if self.figure is not None:
            w, h = self.figure.canvas.get_width_height()

            canvas_img = np.fromstring(self.figure.canvas.tostring_rgb(), dtype=np.uint8, sep='')
            if (canvas_img.shape[0]/3) == 4*h*w:
                canvas_img = canvas_img.reshape(2*h, 2*w, 3)
            elif (canvas_img.shape[0]/3) == (2*h)*(2*w+1):
                canvas_img = canvas_img.reshape(2*h, 2*w+1, 3)
            elif (canvas_img.shape[0]/3) == (2*h+1)*(2*w):
                canvas_img = canvas_img.reshape(2*h+1, 2*w, 3)
            elif (canvas_img.shape[0]/3) == h*w:
                canvas_img = canvas_img.reshape(h, w, 3)
            else:
                canvas_img = np.zeros((600, 600, 3), float)

            canvas_img = np.array(Image.fromarray(canvas_img).resize((600, 600)))
        else:
            canvas_img = np.zeros((600, 600, 3), float)

        self.image = [[[rgb for rgb in c] for c in r] for r in canvas_img]
        return self.image

    def onTimeChanged(self, value):
        if value in self.df:
            time_changed =  value != self.current_time
            self.current_time = value
            if time_changed:
                self.update()
