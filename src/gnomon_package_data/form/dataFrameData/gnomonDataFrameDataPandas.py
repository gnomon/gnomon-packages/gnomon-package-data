import pickle
from base64 import b64encode, b64decode

import gnomon.core
from gnomon.core import gnomonAbstractDataFrameData, gnomonDataFrame

import pandas as pd

from copy import deepcopy
import numpy as np

from gnomon.utils import formDataPlugin

@formDataPlugin(version="1.0.0", coreversion="1.0.0", data_setter='set_dataframe', data_getter='get_dataframe', name='Pandas DataFrame')
class gnomonDataFrameDataPandas(gnomonAbstractDataFrameData):

    def __init__(self, dataFrame=None):
        super().__init__()
        if dataFrame is None:
            self._df = pd.DataFrame()
        else:
            self._df = deepcopy(dataFrame._df)

    def __del__(self):
        del self._df

    def set_dataframe(self, df: pd.DataFrame):
        self._df = deepcopy(df)

    def get_dataframe(self) -> pd.DataFrame:
        return self._df

    def clone(self):
        _clone = gnomonDataFrameDataPandas(self)
        _clone.__disown__()
        return _clone

    # Metadata
    def metadata(self):
        metadata = {}
        metadata['Number of lines'] = str(len(self._df))
        for c in self._df.columns:
            metadata['Column '+c] = str(self._df[c].values.dtype)
        return metadata

    def fromGnomonForm(self, form: gnomonDataFrame):
        df = pd.DataFrame(index=form.index())
        for column in form.columnNames():
            df[column] = form.column(column)
        self.set_dataframe(df)

    def dataName(self):
        return "pandas.DataFrame"

    def index(self):
        return list(self._df.index.values)

    def columnNames(self):
        return list(self._df.columns)

    def columns(self, columnName):
        return dict(zip(self._df.index, self._df[columnName].values))

    def serialize(self):
        return b64encode(pickle.dumps(self._df)).decode("ascii")

    def deserialize(self, serialization: str):
        self._df = pickle.loads(b64decode(serialization.encode("ascii")))
