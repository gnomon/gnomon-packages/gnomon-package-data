import pickle
from base64 import b64encode, b64decode

import numpy as np

from copy import deepcopy

import gnomon.core
from gnomon.core import gnomonAbstractDataDictData, gnomonDataDict, QVariant
from gnomon.utils import formDataPlugin


@formDataPlugin(version="1.0.0", coreversion="1.0.0", data_setter='set_data', data_getter='get_data', name="NumPy Dict")
class gnomonNumpyDataDictData(gnomonAbstractDataDictData):

    def __init__(self, data=None, data_name=""):
        super().__init__()
        self._data = {}
        if data is not None:
            if isinstance(data._data, dict):
                self.add_data(data._data)
            else:
                self.add_data({data_name: data._data})

    def __del__(self):
        pass

    def add_data(self, data):
        if not isinstance(data, dict):
            data = {"": data}
            
        self._data.update({k: deepcopy(v) for k, v in data.items()})

    def set_data(self, data: dict):
        self._data = {}
        self.add_data(data)

    def get_data(self) -> dict:
        return self._data

    def clone(self):
        _clone = gnomonNumpyDataDictData(self)
        _clone.__disown__()
        return _clone

    def fromGnomonForm(self, form: gnomonDataDict):
        data = {}
        for key in form.keys():
            data[key] = form.get(key)
        self.set_data(data)

    # Metadata

    def metadata(self):
        metadata = {}
        metadata["Number of keys"] = str(len(self._data))
        return metadata

    def dataName(self):
        return "{str: numpy.ndarray}"

    def keys(self):
        return list(self._data.keys())

    def get(self, key):
        if key in self._data.keys():
            var = QVariant()
            var.setValue(self._data[key])
            return var
        else:
            print("no data with key:", key , " available keys", self._data.keys())
            return QVariant()

    def set(self, key: str, value: str):
        self._data[key] = value

    def serialize(self):
        return b64encode(pickle.dumps(self._data)).decode("ascii")

    def deserialize(self, serialization: str):
        self._data = pickle.loads(b64decode(serialization.encode("ascii")))
