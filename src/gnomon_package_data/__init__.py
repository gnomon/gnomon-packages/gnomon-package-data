from . import version

__version__ = version.__version__

# ---- metadata --------------------------------------------------------------------------------------------------------

package = "gnomon-package-data"
conda_channel = "gnomon"
