import os
import logging

import numpy as np
import json

import gnomon.core
from gnomon.core import gnomonAbstractDataDictWriter

from gnomon.utils import algorithmPlugin, seriesWriter
from gnomon.utils.decorators import dataDictInput

class NumpyArrayEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return {"ndarray": str(obj.dtype),
                    "value": obj.tolist()}
        return json.JSONEncoder.default(self, obj)


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="JSON Dict Writer")
@dataDictInput("data", data_plugin="gnomonNumpyDataDictData")
@seriesWriter("data", "filepath")
class gnomonDataDictWriterJson(gnomonAbstractDataDictWriter):
    """Write a JSON file into a DataDict.

    The function write a .json file
    """

    def __init__(self):
        super().__init__()
        self.filepath = None

        self.data = {}

    def setPath(self, filepath):
        self.filepath = filepath

    def writeToFile(self, filename, data):
        with open(filename, 'w', encoding='utf-8') as outFile:
            # 2 write data
            json.dump(data, outFile, cls=NumpyArrayEncoder, sort_keys=True, indent=4, ensure_ascii=False)
            

    def run(self):
        if len(self.data) > 1:
            (dirname, filename) = os.path.split(self.filepath)
            dirpath = dirname + "/" + os.path.splitext(filename)[0]
            if not os.path.exists(dirpath):
                os.makedirs(dirpath)

            for time in self.data.keys():
                time_filepath = os.path.join(dirpath, os.path.splitext(filename)[0] + "_t" + "%05.2f" % (time) + ".json")
                self.writeToFile(time_filepath , self.data[time])

        else:
            time = list(self.data.keys())[0]
            self.writeToFile(self.filepath, self.data[time])

    def extensions(self):
        return ["json"]