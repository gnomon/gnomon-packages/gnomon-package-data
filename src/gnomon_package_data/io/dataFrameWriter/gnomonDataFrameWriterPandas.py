import os
import logging

import pandas as pd

import gnomon.core
from gnomon.core import gnomonAbstractDataFrameWriter

from gnomon.utils import algorithmPlugin, seriesWriter
from gnomon.utils.decorators import dataFrameInput

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Pandas CSV Writer")
@dataFrameInput("df", data_plugin="gnomonDataFrameDataPandas")
@seriesWriter("df", "filepath")
class gnomonDataFrameWriterPandas(gnomonAbstractDataFrameWriter):
    def __init__(self):
        super().__init__()
        self.filepath = None

        self.df = {}

    def setPath(self, filepath):
        self.filepath = filepath

    def run(self):
        if len(self.df) > 1:
            (dirname, filename) = os.path.split(self.filepath)
            dirpath = dirname + "/" + os.path.splitext(filename)[0]
            if not os.path.exists(dirpath):
                os.makedirs(dirpath)

            for time in self.df.keys():
                time_filepath = os.path.join(dirpath, os.path.splitext(filename)[0] + "_t" + "%05.2f" % (time) + ".csv")
                self.df[time].to_csv(time_filepath, index=False)
        else:
            time = list(self.df.keys())[0]
            self.df[time].to_csv(self.filepath, index=False)

    def extensions(self):
        return ["csv"]