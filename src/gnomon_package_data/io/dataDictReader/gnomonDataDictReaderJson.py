import os

import numpy as np
import json

import gnomon.core
from gnomon.core import gnomonAbstractDataDictReader

from gnomon.utils import algorithmPlugin, seriesReader
from gnomon.utils.decorators import dataDictOutput

class NumpyArrayDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        json.JSONDecoder.__init__(self, object_hook=self.object_hook, *args, **kwargs)

    def object_hook(self, obj):
        if 'ndarray' not in obj:
            return obj
        dtype = obj['ndarray']
        if 'int' in dtype: 
            return np.asarray(obj['value'], dtype=np.int32)
        else:
            return np.asarray(obj['value'], dtype=np.float64)

@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="JSON Dict Reader")
@dataDictOutput("data", data_plugin="gnomonNumpyDataDictData")
@seriesReader("data", "path")
class gnomonDataDictReaderJson(gnomonAbstractDataDictReader):
    """Load a JSON file into a DataDict.

    The function reads a .json file 
    """
    def __init__(self):
        super().__init__()

        self.data = {}

        self.path = None

    def __del__(self):
        pass

    def run(self):

        self.data = {}

        paths = self.path.split(",")
        for time, path in enumerate(paths):
            if os.path.exists(path) and any([path.endswith(e) for e in self.extensions()]):
                with open(path) as json_file:
                    self.data[time] = json.load(json_file, cls=NumpyArrayDecoder)

    def setPath(self, path):
        self.path = path

    def extensions(self):
        return ["json"]
