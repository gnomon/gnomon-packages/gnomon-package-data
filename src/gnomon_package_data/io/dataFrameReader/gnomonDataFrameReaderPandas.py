import os

import pandas as pd

import gnomon.core
from gnomon.core import gnomonAbstractDataFrameReader

from gnomon.utils import algorithmPlugin, seriesReader
from gnomon.utils.decorators import dataFrameOutput


@algorithmPlugin(version="1.0.0", coreversion="1.0.0", name="Pandas CSV Reader")
@dataFrameOutput("df", data_plugin="gnomonDataFrameDataPandas")
@seriesReader("df", "path")
class gnomonDataFrameReaderPandas(gnomonAbstractDataFrameReader):
    """Load a CSV file into a DataFrame.

    The function reads a .csv file with named columns as a
    data frame structure.
    """
    def __init__(self):
        super().__init__()

        self.df = {}

        self.path = None

    def __del__(self):
        pass

    def run(self):

        self.df = {}

        paths = self.path.split(",")
        for time, path in enumerate(paths):
            if os.path.exists(path) and any([path.endswith(e) for e in self.extensions()]):
                self.df[time] = pd.read_csv(path)

    def setPath(self, path):
        self.path = path

    def extensions(self):
        return ["csv"]
