# ChangeLog

## version 1.0.0 - 2024-03-27
* gnomon 1.0.0
* manage time in 2D visualization plugins

## version 0.5.0 - 2023-07-07
* gnomon 0.81.0
* QML visualization plugin for data dict

## version 0.4.2 - 2023-04-12
* gnomon 0.80.0
* refresh parameters

## version 0.3.2 - 2023-02-03
* gnomon 0.72.0
* plugin metadata
* set method for datadict

## version 0.3.1 - 2022-09-15
* gnomon 0.71.0
* enhance data visu plugins

## version 0.3.0 - 2022-07-19
* gnomon 0.70.1
* multiples files for readers
* new decorators for readers
* bugfix

## version 0.2.0 - 2022-03-15
* python 3.9
* gnomon 0.60
