#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

short_descr = "Define, read, write and visualize data containers"
readme = open('README.md').read()


# find packages
pkgs = find_packages('src')

setup_kwds = dict(
    name='gnomon_package_data',
    version="1.0.0",
    description=short_descr,
    long_description=readme,
    author="gnomon-dev",
    author_email="amdt-gnomon-dev@inria.fr",
    url='',
    license='LGPL-3.0',
    zip_safe=False,

    packages=pkgs,

    package_dir={'': 'src'},
    package_data={
        "": [
            "*.png",
            "*/*.png",
            "*/*/*.png",
            "*.json",
            "*/*.json",
            "*/*/*.json"
        ]
    },
    entry_points={
        'dataFrameMplVisualization': [
            # 'gnomonMatplotlibVisulalizationDataFrame = gnomon_package_data.visualization.dataFrameMplVisualization.gnomondataFrameMplVisualization',
            'matplotlibScatterPandas = gnomon_package_data.visualization.dataFrameMplVisualization.matplotlibScatterPandas',
            'matplotlibBoxplotsPandas = gnomon_package_data.visualization.dataFrameMplVisualization.matplotlibBoxplotsPandas',
        ],
        'dataDictQmlVisualization': [
            'dictTextValue = gnomon_package_data.visualization.dataDictQmlVisualization.dictTextValue'
        ],
        'dataFrameData': [
            'gnomonDataFrameDataPandas = gnomon_package_data.form.dataFrameData.gnomonDataFrameDataPandas'
        ],
        'dataDictData': [
            'gnomonNumpyDataDictData = gnomon_package_data.form.dataDictData.gnomonNumpyDataDictData'
        ],
        'dataDictReader': [
            'gnomonDataDictReaderJson = gnomon_package_data.io.dataDictReader.gnomonDataDictReaderJson'
        ],
        'dataFrameWriter': [
            'gnomonDataFrameWriterPandas = gnomon_package_data.io.dataFrameWriter.gnomonDataFrameWriterPandas'
        ],
        'dataDictWriter': [
            'gnomonDataDictWriterJson = gnomon_package_data.io.dataDictWriter.gnomonDataDictWriterJson'
        ],
        'dataFrameReader': [
            'gnomonDataFrameReaderPandas = gnomon_package_data.io.dataFrameReader.gnomonDataFrameReaderPandas'
        ]
    },
    keywords='',

    test_suite='nose.collector',
    )

setup(**setup_kwds)
